﻿using System.Web.Optimization;
using TestApplication.Website.App_Start;

namespace TestApplication.Website
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = false;

            bundles.IgnoreList.Clear();
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*intellisense.js");

            // Modernizr
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/lib/modernizr-*"));


            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery",
                "//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js")
                .Include("~/Scripts/lib/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/knockout")
                .Include(
                    // Knockout and its plugins
                    "~/Scripts/lib/knockout-{version}.js",
                    "~/Scripts/lib/knockout.activity.js",
                    "~/Scripts/lib/knockout.asyncCommand.js",
                    "~/Scripts/lib/knockout.dirtyFlag.js",
                    "~/Scripts/lib/knockout.validation.js",
                    "~/Scripts/lib/koExternalTemplateEngine.js"

                    ));

            bundles.Add(new ScriptBundle("~/bundles/javascriptlibs")
                .Include(
                    "~/Content/vendor/jquery/jquery.js",
                    "~/Content/vendor/jquery.appear/jquery.appear.js",
                    "~/Content/vendor/jquery.easing/jquery.easing.js",
                    "~/Content/vendor/jquery-cookie/jquery-cookie.js",
                    "~/Content/vendor/bootstrap/bootstrap.js",
                    "~/Content/vendor/common/common.js",
                    "~/Content/vendor/jquery.validation/jquery.validation.js",
                    "~/Content/vendor/jquery.stellar/jquery.stellar.js",
                    "~/Content/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js",
                    "~/Content/vendor/jquery.gmap/jquery.gmap.js",
                    "~/Content/vendor/isotope/jquery.isotope.js",
                    "~/Content/vendor/owlcarousel/owl.carousel.js",
                    "~/Content/vendor/jflickrfeed/jflickrfeed.js",
                    "~/Content/magnific-popup/jquery.magnific-popup.js",
                    "~/Content/vendor/vide/vide.js",
                    "~/Content/js/theme.js",
                    "~/Content/js/custom.js",
                    "~/Content/js/theme.init.js"));

            // CSS
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/animate.css",
                "~/Content/css/blog.css",
                "~/Content/css/elements.css",
                "~/Content/css/ie.css",
                "~/Content/css/shop.css",
                "~/Content/css/style.css",
                "~/Content/css/skins/default.css"));

            // Vendor CSS
            bundles.Add(new StyleBundle("~/Content/vendorcss").Include(
                "~/Content/vendor/bootstrap/bootstrap.css",
                "~/Content/vendor/fontawesome/css/font-awesome.css",
                "~/Content/vendor/owlcarousel/owl.carousel.min.css",
                "~/Content/vendor/owlcarousel/owl.theme.default.min.css",
                "~/Content/vendor/magnific-popup/magnific-popup.css"));
        }
    }
}
