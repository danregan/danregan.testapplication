﻿var teacherRegisterViewModel;


function Teacher(id, firstName, lastName, emailAddress) {
    var self = this;

    self.Id = ko.observable(id);
    self.FirstName = ko.observable(firstName);
    self.LastName = ko.observable(lastName);
    self.EmailAddress = ko.observable(emailAddress);

    self.addTeacher = function () {
        var dataObject = ko.toJSON(this);

        $.ajax({
            url: '/api/Teachers',
            type: 'post',
            data: dataObject,
            contentType: 'application/json',
            success: function (data) {
                teacherRegisterViewModel.teacherListViewModel.teachers.push(new Teacher(data.FirstName, data.LastName, data.EmailAddress));

                self.FirstName('');
                self.LastName('');
                self.EmailAddress('');

                teacherRegisterViewModel.teacherListViewModel.getTeachers();
            }
        });
    };
}


function TeacherList() {
    var self = this;

    self.teachers = ko.observableArray([]);

    self.getTeachers = function () {
        self.teachers.removeAll();


        $.getJSON('/api/Teachers', function (data) {
            $.each(data, function (key, value) {
                self.teachers.push(new Teacher(value.id, value.firstName, value.lastName, value.emailAddress));
            });
        });
    };


    self.removeTeacher = function (teacher) {
        $.ajax({
            url: '/api/Teachers/' + teacher.Id(),
            type: 'delete',
            contentType: 'application/json',
            success: function () {
                self.teachers.remove(teacher);
                teacherRegisterViewModel.teacherListViewModel.getTeachers();
            },
            complete: function (xhr, textStatus) {
                if(xhr.status == 403)
                {
                    alert("Forbidden - Violates FK Restraint");
                }
            }
        });
    };
}


teacherRegisterViewModel = { addTeacherViewModel: new Teacher(), teacherListViewModel: new TeacherList() };


$(document).ready(function () {

    ko.applyBindings(teacherRegisterViewModel);

    teacherRegisterViewModel.teacherListViewModel.getTeachers();
});