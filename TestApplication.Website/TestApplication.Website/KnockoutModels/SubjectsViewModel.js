﻿var subjectRegisterViewModel;


function Subject(id, name) {
    var self = this;

    self.Id = ko.observable(id);
    self.Name = ko.observable(name);

    self.addSubject = function () {
        var dataObject = ko.toJSON(this);

        $.ajax({
            url: '/api/Subjects',
            type: 'post',
            data: dataObject,
            contentType: 'application/json',
            success: function (data) {
                subjectRegisterViewModel.subjectListViewModel.subjects.push(new Subject(data.Name));

                self.Name('');
                subjectRegisterViewModel.subjectListViewModel.getSubjects();
            }
        });
    };
}

function SubjectList() {
    var self = this;

    self.subjects = ko.observableArray([]);

    self.getSubjects = function () {
        self.subjects.removeAll();

        $.getJSON('/api/Subjects', function (data) {
            $.each(data, function (key, value) {
                self.subjects.push(new Subject(value.id, value.name));
            });
        });
    };

    self.removeSubject = function (subject) {
        $.ajax({
            url: '/api/Subjects/' + subject.Id(),
            type: 'delete',
            contentType: 'application/json',
            success: function () {
                self.subjects.remove(subject);
                subjectRegisterViewModel.subjectListViewModel.getSubjects();
            },
            complete: function (xhr, textStatus) {
                if(xhr.status == 403)
                {
                    alert("Forbidden - Violates FK Restraint");
                }
            }
        });

        
    };
}

subjectRegisterViewModel = { addSubjectViewModel: new Subject(), subjectListViewModel: new SubjectList() };

$(document).ready(function () {

    ko.applyBindings(subjectRegisterViewModel);

    subjectRegisterViewModel.subjectListViewModel.getSubjects();
});