﻿var lessonRegisterViewModel;

var Teacher = function(id, name)
{
    var self = this;

    self.Id = id;
    self.Name = name;
}

var x = "";

function getTeachers() {
    var teachers = [];

    $.ajax({
        url: '/api/Teachers',
        async: false,
        dataType: 'json',
        success: function (json) {
            for(key in json)
            {
                teachers[key] = new Teacher(json[key].id, json[key].firstName + ' ' + json[key].lastName);
            }

        }
    });

    return teachers;
}

var xTeachers = getTeachers();

console.log(xTeachers)


function Lesson(id, name, teacher, classroom, subject, startTime, endTime)
{
    var self = this;

    self.Id = ko.observable(id);
    self.Name = ko.observable(name);
    self.Teacher = ko.observable(teacher);
    self.Classroom = ko.observable(classroom);
    self.Subject = ko.observable(subject);
    self.StartTime = ko.observable(startTime);
    self.EndTime = ko.observable(endTime);

    self.teachers = xTeachers;

    self.addLesson = function () {
        var dataObject = ko.toJSON(this);


        $.ajax({
            url: '/api/Lessons',
            type: 'post',
            data: dataObject,
            contentType: 'application/json',
            success: function (data) {
                lessonRegisterViewModel.lessonListViewModel.lessons.push(new Lesson(data.Name, data.Teacher, data.Classroom, data.Subject, data.StartTime, data.EndTime));

                self.Name('');
                self.Teacher('');
                self.Classroom('');
                self.Subject('');
                self.StartTime('');
                self.EndTime('');

                lessonRegisterViewModel.lessonListViewModel.getLessons();
            }
        });
    }
}

function LessonList() {
    var self = this;
    self.lessons = ko.observableArray([]);

    self.getLessons = function () {
        self.lessons.removeAll();

        $.getJSON('/api/Lessons', function (data) {
            $.each(data, function (key, value) {
                self.lessons.push(new Lesson(value.id, value.name, value.teacher, value.classroom, value.subject, value.startTime, value.endTime));
            });
        });


    };

    self.removeLesson = function(lesson)
    {
        $.ajax({
            url: '/api/Lessons/' + lesson.Id(),
            type: 'delete',
            contentType: 'application/json',
            success: function () {
                self.lessons.remove(lesson);
                lessonRegisterViewModel.lessonListViewModel.getLessons();
            },
            complete: function (xhr, textStatus) {
                if (xhr.status == 403) {
                    alert("Forbidden - Violates FK Restraint");
                }
            }
        });

        
    }
}

lessonRegisterViewModel = { addLessonViewModel: new Lesson(), lessonListViewModel: new LessonList() };

$(document).ready(function () {
    // bind view model to referring view
    ko.applyBindings(lessonRegisterViewModel);

    lessonRegisterViewModel.lessonListViewModel.getLessons();
});