﻿var classroomRegisterViewModel;


function Classroom(id, name) {
    var self = this;

    self.Id = ko.observable(id);
    self.Name = ko.observable(name);

    self.addClassroom = function () {
        var dataObject = ko.toJSON(this);

        $.ajax({
            url: '/api/Rooms',
            type: 'post',
            data: dataObject,
            contentType: 'application/json',
            success: function (data) {
                classroomRegisterViewModel.classroomListViewModel.classrooms.push(new Classroom(data.Name));

                self.Name('');
                classroomRegisterViewModel.classroomListViewModel.getClassrooms();
            }
        });
    };
}

function ClassroomList() {
    var self = this;

    self.classrooms = ko.observableArray([]);

    self.getClassrooms = function () {
        self.classrooms.removeAll();

        $.getJSON('/api/Rooms', function (data) {
            $.each(data, function (key, value) {
                self.classrooms.push(new Classroom(value.id, value.name));
            });
        });
    };

    self.removeClassroom = function (classroom) {
        $.ajax({
            url: '/api/Rooms/' + classroom.Id(),
            type: 'delete',
            contentType: 'application/json',
            success: function () {
                self.classrooms.remove(classroom);
                classroomRegisterViewModel.classroomListViewModel.getClassrooms();
            },
            complete: function (xhr, textStatus) {
                if(xhr.status == 403)
                {
                    alert("Forbidden - Violates FK Restraint");
                }
            }
        });

        
    };
}

classroomRegisterViewModel = { addClassroomViewModel: new Classroom(), classroomListViewModel: new ClassroomList() };

$(document).ready(function () {

    ko.applyBindings(classroomRegisterViewModel);

    classroomRegisterViewModel.classroomListViewModel.getClassrooms();
});