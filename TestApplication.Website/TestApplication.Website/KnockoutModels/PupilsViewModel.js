﻿var pupilRegisterViewModel;

function Pupil(id, firstName, lastName) {
    var self = this;

    self.Id = ko.observable(id);
    self.FirstName = ko.observable(firstName);
    self.LastName = ko.observable(lastName);

    self.addPupil = function () {
        var dataObject = ko.toJSON(this);

        $.ajax({
            url: '/api/Pupils',
            type: 'post',
            data: dataObject,
            contentType: 'application/json',
            success: function (data) {
                pupilRegisterViewModel.pupilListViewModel.pupils.push(new Pupil(data.FirstName, data.LastName));

                self.FirstName('');
                self.LastName('');

                pupilRegisterViewModel.pupilListViewModel.getPupils();
            }
        });
    };
}


function PupilList() {
    var self = this;

    self.pupils = ko.observableArray([]);

    self.getPupils = function () {
        self.pupils.removeAll();

        $.getJSON('/api/Pupils', function (data) {
            $.each(data, function (key, value) {
                self.pupils.push(new Pupil(value.id, value.firstName, value.lastName));
            });
        });
    };

    self.removePupil = function (pupil) {
        $.ajax({
            url: '/api/Pupils/' + pupil.Id(),
            type: 'delete',
            contentType: 'application/json',
            success: function () {
                self.pupils.remove(pupil);
                pupilRegisterViewModel.pupilListViewModel.getPupils();
            },
            complete: function (xhr, textStatus) {
                if(xhr.status == 403)
                {
                    alert("Forbidden - Violates FK Restraint");
                }
            }
        });


    };
}

pupilRegisterViewModel = { addPupilViewModel: new Pupil(), pupilListViewModel: new PupilList() };

$(document).ready(function () {
    ko.applyBindings(pupilRegisterViewModel);

    pupilRegisterViewModel.pupilListViewModel.getPupils();
});