﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestApplication.DataContracts;
using TestApplication.Model;

namespace TestApplication.Website.Controllers
{
    public class TeacherSubjectsController : ApiControllerBase
    {
        public TeacherSubjectsController(ITestApplicationUOW uow)
        {
            Uow = uow;
        }

        // Read all
        public IEnumerable<TeacherSubjects> Get()
        {
            return Uow.TeacherSubjects.GetAll();
        }
    }
}

