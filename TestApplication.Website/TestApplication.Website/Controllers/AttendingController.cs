﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestApplication.DataContracts;
using TestApplication.Model;

namespace TestApplication.Website.Controllers
{
    public class AttendingController : ApiControllerBase
    {
        public AttendingController(ITestApplicationUOW uow)
        {
            Uow = uow;
        }

        // Read all
        public IEnumerable<Attending> Get()
        {
            return Uow.Attending.GetAll();
        }
    }

    public class AttendingPupilController : ApiControllerBase
    {
        public AttendingPupilController(ITestApplicationUOW uow)
        {
            Uow = uow;
        }

        // Read all
        public IEnumerable<Attending> Get()
        {
            return Uow.Attending.GetAll();
        }

        public IQueryable<Attending> GetByPupilId(int id)
        {
            var attending = Uow.Attending.GetByPupilId(id);
            if (attending != null) return attending;
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
        }
    }

    public class AttendingLessonController : ApiControllerBase
    {
        public AttendingLessonController(ITestApplicationUOW uow)
        {
            Uow = uow;
        }

        // Read all
        public IEnumerable<Attending> Get()
        {
            return Uow.Attending.GetAll();
        }

        public IQueryable<Attending> GetByLessonId(int id)
        {
            var attending = Uow.Attending.GetByLessonId(id);
            if (attending != null) return attending;
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
        }
    }


}

