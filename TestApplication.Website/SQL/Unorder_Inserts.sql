INSERT INTO Lesson (Name, StartTime, EndTime, Classroom_Id, Subject_Id, Teacher_Id)
VALUES ('Lesson 1', '2016-02-10 09:30:00.000', '2016-02-10 10:30:00.000', 1, 4, 3),
('Lesson 2', '2016-02-10 09:00:00.000', '2016-02-10 10:30:00.000', 2, 3, 4),
('Lesson 3', '2016-02-10 10:30:00.000', '2016-02-10 14:30:00.000', 3, 4, 5),
('Lesson 4', '2016-02-10 16:30:00.000', '2016-02-10 17:30:00.000', 1, 1, 1)

INSERT INTO TeacherSubjects
VALUES (1, 1), (1, 2), (2, 3), (2, 1), (4, 4), (4, 2)

INSERT INTO RoomLessons
VALUES (1, 3), (2, 2), (3, 4), (4, 1)

INSERT INTO Attending
VALUES (1, 1), (1, 2), (1, 3), (2, 4), (2, 1), (5, 3)