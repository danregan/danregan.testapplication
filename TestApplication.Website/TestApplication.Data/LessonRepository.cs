﻿using System;
using System.Data.Entity;
using System.Linq;
using TestApplication.DataAccessLayer;
using TestApplication.DataContracts;
using TestApplication.Model;

namespace TestApplication.Data
{
    public class LessonRepository : EntityRepository<Lesson>, ILessonRepository
    {
        public LessonRepository(DbContext context) : base(context) { }

        public override IQueryable<Lesson> GetAll()
        {
            return base.GetAll().Include(a => a.Classroom)
                .Include(b => b.Teacher)
                .Include(c => c.Subject);
        }
    }
}
