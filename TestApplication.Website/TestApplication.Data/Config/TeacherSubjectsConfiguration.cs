﻿using System.Data.Entity.ModelConfiguration;
using TestApplication.Model;

namespace TestApplication.DataAccessLayer.Config
{
    public class TeacherSubjectsConfiguration : EntityTypeConfiguration<TeacherSubjects>
    {
        public TeacherSubjectsConfiguration()
        {
            HasKey(x => new { x.TeacherId, x.SubjectId });
        }
    }
}
