﻿using System.Data.Entity.ModelConfiguration;
using TestApplication.Model;

namespace TestApplication.DataAccessLayer.Config
{
    public class RoomLessonsConfiguration : EntityTypeConfiguration<RoomLessons>
    {
        public RoomLessonsConfiguration()
        {
            HasKey(x => new { x.RoomId, x.LessonId });
        }
    }
}
