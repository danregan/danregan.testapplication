﻿using System.Data.Entity.ModelConfiguration;
using TestApplication.Model;

namespace TestApplication.DataAccessLayer.Config
{
    public class AttendingConfiguration : EntityTypeConfiguration<Attending>
    {
        public AttendingConfiguration()
        {
            HasKey(x => new { x.PupilId, x.LessonId });

        }
    }
}
