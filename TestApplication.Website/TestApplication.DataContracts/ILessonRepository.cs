﻿using System.Linq;
using TestApplication.Model;

namespace TestApplication.DataContracts
{
    public interface ILessonRepository : IRepository<Lesson>
    {
    }
}
